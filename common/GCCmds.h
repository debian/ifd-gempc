/*
 *  GCCommands.h
 *  $Id: GCCmds.h,v 1.7 2004-08-08 15:10:19 rousseau Exp $
 *  ifd-GemPC
 *
 *  Created by JL Giraud on Sat Oct 20 2001.
 *  Copyright (c) 2001 Jean-Luc Giraud
 *
 *  License: this code is under a double licence COPYING.BSD and COPYING.GPL
 *
 */


#ifndef _GCCMDS_H_
#define _GCCMDS_H_

ifd_t GCCmdPowerDown(const DWORD Lun);
ifd_t GCCmdPowerUp(const DWORD Lun, PDWORD nlength, UCHAR buffer[]);
ifd_t GCCmdGetOSVersion(const DWORD lun, PDWORD length, UCHAR buffer[]);
ifd_t GCCmdRestart(const DWORD lun);
ifd_t GCCmdConfigureSIOLine(const DWORD lun, const int baudrate);
ifd_t GCCmdCardStatus(const DWORD lun, UCHAR response[], PDWORD length);
ifd_t GCCmdSetMode(const DWORD lun, const int mode);
ifd_t GCMakeCommand(const DWORD Lun, const DWORD nLengthIn,
	const UCHAR pcBufferCmd[], PDWORD pnLengthOut, UCHAR pcBufferOut[],
	gcore_t *response);
UCHAR GCGemCoreError(const UCHAR rv, const char *file, const int line,
	const char *function);

#endif

