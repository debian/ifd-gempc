/*****************************************************************
/
/ File   :   gempc_ifdhandler.h
/ Author :   Ludovic Rousseau <ludovic.rousseau@free.fr>
/ Date   :   22 october 2001
/ Purpose:   This provides reader specific low-level calls.
/ License:   See file LICENSE.BSD
/
/ $Id: gempc_ifdhandler.h,v 1.2 2004-07-03 21:02:42 rousseau Exp $
/
******************************************************************/

#ifndef _gempc_ifd_handler_h_
#define _gempc_ifd_handler_h_

#ifdef __cplusplus
extern "C" {
#endif

#include <ifdhandler.h>

void IFDSetEmv(DWORD lun);

/* do not use RESPONSECODE (long, 64 bits) when 32 bits are enough */
typedef int ifd_t;

typedef enum {
	STATUS_SUCCESS               = 0xFA,
	STATUS_UNSUCCESSFUL          = 0xFB,
	STATUS_COMM_ERROR            = 0xFC,
	STATUS_DEVICE_PROTOCOL_ERROR = 0xFD
} status_t;

#define PCSCLITE_MAX_READERS PCSCLITE_MAX_READERS_CONTEXTS

#ifdef __cplusplus
}
#endif

#endif /* _gempc_ifd_hander_h_ */

