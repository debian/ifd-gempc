# $Id: Makefile,v 1.9 2003-05-03 07:43:49 rousseau Exp $

all:
	$(MAKE) -C common clean
	$(MAKE) -C GemPC410
	$(MAKE) -C common clean
	$(MAKE) -C GemPC430

clean:
	$(MAKE) -C GemPC410 clean
	$(MAKE) -C GemPC430 clean
	$(MAKE) -C common clean

distclean:
	$(MAKE) -C GemPC410 distclean
	$(MAKE) -C GemPC430 distclean
	$(MAKE) -C common distclean
	rm -f config.log

install:
	$(MAKE) -C common clean
	$(MAKE) -C GemPC410 install
	$(MAKE) -C common clean
	$(MAKE) -C GemPC430 install

.PHONY: all clean distclean ctags

