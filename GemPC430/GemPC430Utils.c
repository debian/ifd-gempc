/*
 *  GemPC430Utils.c
 *  $Id: GemPC430Utils.c,v 1.20 2009-04-12 09:22:30 rousseau Exp $
 *  GemPC430 dedicated functions
 *
 *  Created by giraud on Sat Oct 20 2001.
 *  Copyright (c) 2001 Jean-Luc Giraud.
 *                2001-2004 Ludovic Rousseau
 *  License:   See file COPYING
 *
 */

#include <stdio.h>

#include "gempc_ifdhandler.h"
#include "Config.h"
#include "GemPC430Utils.h"
#include "GemCore.h"
#include "GCCmds.h"
#include "libusb_wrap.h"
#include "GCdebug.h"
#include "GCTransport.h"


ifd_t OpenGemPC430ByName(DWORD lun, LPSTR dev_name)
{
	UCHAR os_version[IFD_LEN_VERSION+2];
	DWORD length;

    if (OpenUSB(lun, dev_name) != STATUS_SUCCESS)
    {
        DEBUG_CRITICAL("OpenUSB failed");
        return IFD_COMMUNICATION_ERROR;
    }

    /* Set the mode to ROS but no TLP (ATR should then be fine) */
    if ( GCCmdSetMode(lun, IFD_MODE_ROSNOTLP) != IFD_SUCCESS )
    {
        DEBUG_CRITICAL("Setmode failed");
        CloseUSB(lun);
        return IFD_COMMUNICATION_ERROR;
    }

	/* Get the GemCore OS version */
	length = sizeof(os_version);
	if (GCCmdGetOSVersion(lun, &length, os_version) != IFD_SUCCESS)
	{
		DEBUG_CRITICAL("GCCmdGetOSVersion failed");
		return IFD_COMMUNICATION_ERROR;
	}

	/* Not really critical but shall be logged */
	DEBUG_CRITICAL2("OS string: %s", os_version);

	return IFD_SUCCESS;
} /* OpenGemPC430ByName */

ifd_t OpenGemPC430(DWORD lun, DWORD channel)
{
	(void)channel;
	return OpenGemPC430ByName(lun, NULL);
} /* OpenGemPC430 */

ifd_t CloseGemPC430(DWORD lun)
{
    if ( CloseUSB(lun) != STATUS_SUCCESS)
        return IFD_COMMUNICATION_ERROR;

    return IFD_SUCCESS;
} /* CloseGemPC430 */

