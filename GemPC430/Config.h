/*
 *  $Id: Config.h,v 1.14 2005-06-17 10:37:35 rousseau Exp $
 *  ifd-GemPC430
 *
 *  Created by JL Giraud <jl.giraud@free.fr> on Sun Nov 19 2000.
 *  License:   See file COPYING
 *
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

/* Perform automatic PPS negociation
 * unsed by default for safety instead of speed */
/* #define AUTOMATIC_PPS */

/* GemPC430 specific */
 
#define OpenPort OpenGemPC430
#define OpenPortByName OpenGemPC430ByName
#define ClosePort CloseGemPC430
#define ReadPort ReadUSB
#define WritePort WriteUSB

#define READER_NAME "GemPC43x"

#endif

