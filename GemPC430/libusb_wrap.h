/*
 * libusb_wrap.h
 * Id$
 * USB access routines using the libusb library
 *
 * Author: Ludovic Rousseau
 * Copyright (c) 2002 Ludovic Rousseau
 * License: See file COPYING.GPL
 */

#ifndef _LIBUSB_WRAP_
#define _LIBUSB_WRAP_

status_t OpenUSB( DWORD lun, LPSTR device );
status_t WriteUSB( DWORD lun, DWORD length, unsigned char *Buffer );
status_t ReadUSB( DWORD lun, DWORD *length, unsigned char *Buffer );
status_t CloseUSB( DWORD lun );

#endif

