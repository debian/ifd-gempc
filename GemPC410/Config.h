/*
 *  $Id: Config.h,v 1.12 2005-06-17 10:37:38 rousseau Exp $
 *  ifd-GemPC410
 *
 *  Created by JL Giraud <jl.giraud@free.fr> on Sun Nov 19 2000.
 *  Updated by Ludovic Rousseau <ludovic.rousseau@free.fr> Nov 23 2001.
 *  License:   See file COPYING.GPL
 *
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

/* Perform automatic PPS negociation
 * unsed by default for safety instead of speed */
/* #define AUTOMATIC_PPS */

/* GemPC410 specific */

#define OpenPort OpenGemPC410
#define OpenPortByName OpenGemPC410ByName
#define ClosePort CloseGemPC410
#define ReadPort ReadGBP
#define WritePort WriteGBP

#define READER_NAME "GemPC41x"

#endif

