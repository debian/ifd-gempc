/*       Title: gbpserial.h
/       Author: Ludovic Rousseau
/    Copyright: 2001, GNU General Public Licence
/      Purpose: Abstracts GBP API to serial like calls
*/

/* #define STATUS_PCB_ERROR 0xFE */
#define STATUS_PCB_REP_0 0x1000
#define STATUS_PCB_REP_1 0x1001
#define STATUS_PCB_SYNC_REQ 0x1002
#define STATUS_PCB_SYNC_RESP 0x1003
#define STATUS_NAD_UNKNOWN 0x1004

RESPONSECODE OpenGBP(DWORD lun, LPSTR dev_name);
RESPONSECODE WriteGBP(DWORD lun, DWORD length, unsigned char *Buffer);
RESPONSECODE ReadGBP(DWORD lun, unsigned long *length, unsigned char *Buffer);
RESPONSECODE CloseGBP(DWORD lun);
int SetGBPSeqNumber(DWORD lun, int seq);

