/*
 *  GemPC410Utils.h
 *  $Id: GemPC410Utils.h,v 1.5 2012/04/08 19:09:42 rousseau Exp $
 *  GemPC410 dedicated functions
 *
 *  Created by giraud on Sat Oct 20 2001.
 *  Copyright (c) 2001-2004 Jean-Luc Giraud and Ludovic Rousseau
 *  License:   See file COPYING.GPL
 *
 */

#ifndef _GEMPC410UTILS_H_
#define _GEMPC410UTILS_H_

RESPONSECODE OpenGemPC410ByName(DWORD lun, LPSTR dev_name);
RESPONSECODE OpenGemPC410(DWORD lun, DWORD channel);
RESPONSECODE CloseGemPC410(DWORD lun);

#endif

